const items = require('./inventory');

const sortItems = items.sort((item1, item2) => {
    if(item1["contains"].length < item2["contains"].length){
        return -1;
    }
    if(item1["contains"].length > item2["contains"].length){
        return 1;
    }else{
        return 0;
    }
});

console.log(sortItems);